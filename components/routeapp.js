import React, { Component } from 'react';
import { Router, Stack, Scene } from 'react-native-router-flux';
import {StyleProvider, Container} from 'native-base'
import getTheme from './native-base-theme/components/'
import myTheme from './native-base-theme/variables/material'


import AUTHLOGIN from './screen/auth/login';
import SCREENMAIN from './screen/mainmenu';
import SCREENNEWS from './screen/news';
import SCREENPLANNING from './screen/planning';
import SCREENSCHEDULE from './screen/schedule';

import SCREENREPORT from './screen/report/listincident';
import SCREENREPORTFORM from './screen/report/formincident';
import SCREENCAMERA from './screen/report/cameraopen';
import SCREENQRCHECK from './screen/live/scanqr';
import SCREENLIVETRACKING from './screen/live/livetracking'

import SCREENCHIEF from './screen/report/chiefincident'
import SCREENCHIEFDETAIL from './screen/report/chiefdetailincident'

import SCREENCHECKLIVEQR from './screen/live/livecheckqr';
import SCREENSOS from './screen/sos/formsos';


console.disableYellowBox = true
export default class RouteApp extends Component {
	render() {
		return (
			<StyleProvider style={getTheme(myTheme)}>
			<Router>

				<Stack key="root">
					<Scene key="login" component={AUTHLOGIN} initial={true} title="Sign in" hideNavBar={true} />
					<Scene key="mainmenu" component={SCREENMAIN} title="Home" hideNavBar={true} />

					<Scene key="report" component={SCREENREPORT} title="Report" hideNavBar={true} />
					<Scene key="reportform" component={SCREENREPORTFORM} title="Report" hideNavBar={true} />
					<Scene key="camera" component={SCREENCAMERA} title="Report" hideNavBar={true} />

					{/* CHEIF */}
					<Scene key="chief" component={SCREENCHIEF} title="Report" hideNavBar={true} />
					<Scene key="chiefdetail" component={SCREENCHIEFDETAIL} title="Report" hideNavBar={true} />

					<Scene key="livecheck" component={SCREENCHECKLIVEQR} title="Report" hideNavBar={true} />
					<Scene key="livetracking" component={SCREENLIVETRACKING} title="Report" hideNavBar={true} />
					<Scene key="qrcheck" component={SCREENQRCHECK} title="Report" hideNavBar={true} />

					<Scene key="listsos" component={SCREENSOS} title="Report" hideNavBar={true} />

					<Scene key="newspaper" component={SCREENNEWS} title="News" />
					<Scene key="planningkerja" component={SCREENPLANNING} title="Planning" />
					<Scene key="jadwalkegiatan" component={SCREENSCHEDULE} title="Schedule" />



				</Stack>
			</Router>
			 </StyleProvider>
		)
	}
}
