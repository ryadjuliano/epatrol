import React, { Component } from 'react';
import { Text, ImageBackground } from 'react-native';
import { Content, Card, CardItem, Body, Left, Right, List, ListItem, Header, Button, Icon, Title } from 'native-base';
import { Actions } from 'react-native-router-flux';

export default class AppBodyData extends Component {

    mainmenu() {
        Actions.menupeserta()
    }
    render() {
        let articles = this.props.data.map(function (articleData, index) {
            // let jams = articleData.jam.map(function(datas, indecx))
            return (

                <Card>
                    <CardItem header>
                        <Left>
                            <Body>
                                <Text>{articleData.lokasi}</Text>
                                {/* <Text note>5 November 2018</Text> */}
                            </Body>
                        </Left>
                        <Right>
                            <Body>
                                <Text>{articleData.tanggal}</Text></Body>
                        </Right>
                    </CardItem>
                    <CardItem>
                        <Left>
                            <Body>
                                <Text>{articleData.jam}</Text>
                                {/* <List>
                            <ListItem>
                            <Text>{articleData.jam}</Text>
                            </ListItem>
                        </List> */}
                                <Text note>{articleData.lokasi}</Text>
                            </Body>
                        </Left>
                    </CardItem>
                </Card>

            )
        });

        return (

            <Content>
                 <Header style={{ backgroundColor: '#00819e' }}>
                    <Left>
                        <Button transparent onPress={this.mainmenu}>
                            <Icon name='arrow-back' style={{ color: '#000' }} />
                        </Button>
                    </Left>
                    <Body>
                        <Title style={{ color: '#fff' }}>Info Kereta</Title>
                    </Body>
                    <Right>
                        <Button transparent>
                            {/* <Icon name='menu' /> */}
                        </Button>
                    </Right>
                </Header>
                
                {articles}
            </Content>

        );

    }
}
module.export = AppBodyData;