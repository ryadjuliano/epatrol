
import React, {Component} from 'react';
import {Image, StyleSheet,AsyncStorage} from 'react-native';
import {Footer, FooterTab, Icon, Button, Text} from 'native-base';

import {Actions} from 'react-native-router-flux';
import { OutLogCheck } from './screen/constants/services'
export default class AppFooter extends Component {
  constructor() {
    super();
    this.state = {
      activeTabName: 'feed'
    };
  }

  tabAction(tab) {
    this.setState({activeTabName: tab});
    alert(this.state.activeTabName)
    if (tab === 'feed') {
      Actions.dashboard();
    } else if (tab === 'news') {
      Actions.history();
    } else if (tab === 'eticket') {
      Actions.eticketlist();
    } else if (tab === 'account') {
      Actions.account()
    } else {
      Actions.infomore();
    }
  }

  home () {
    Actions.mainmenu()
  }

  listincident = async () => {
    
    // 
    const role = await AsyncStorage.getItem("roleaccess")
    if (role == 1) {
      Actions.report()
    } else if (role == 2) {
      Actions.chief()
    } else if (role == 3) {
      Actions.report()
    } else if (role == 4) {
      Actions.chief()
    }
  }

  liveMenu = async () => {

    const role = await AsyncStorage.getItem("roleaccess")
    // alert(role)
    if (role == 1) {
      Actions.livetracking()
    } else if (role == 2) {
      Actions.livecheck()
    } else if (role == 3) {
      Actions.livecheck()
    } else if (role == 4) {
      Actions.livetracking()
    }
    // Actions.livecheck()
    // Actions.livetracking()
  }

  ListSos  () {
    Actions.listsos()
  }

  Logout = async () =>{
    
    const idUser = await AsyncStorage.getItem("idUser")
    // OutLogCheck
      const params = {
        idUser: idUser,
        kondisi: 'logout'
      }
      console.log(params)
      OutLogCheck(params).then((result) => {
        console.log(result)
        if (result.res.status == 1) {
          AsyncStorage.removeItem("statusUser")
          AsyncStorage.removeItem("roleaccess")
          AsyncStorage.removeItem("idUser")
          AsyncStorage.removeItem("pola")
          Actions.login()
        } else {
          console.log('error')
        }
      
      })
  }

  
  
  render() {
    return (
      <Footer style={{backgroundColor:'black'}}>
          <FooterTab style={{backgroundColor:'black'}}>
            <Button  onPress={this.home}>
              <Icon name="home" style={{color:'#ffd04d'}} />
              <Text style={{ fontSize: 8 }}>Home</Text>
            </Button>
            <Button onPress={this.listincident}>
              <Icon name="flame" style={{color:'#ffd04d'}}/>
              <Text style={{ fontSize: 8 }}>Incident</Text>
            </Button>
            <Button vertical onPress={this.liveMenu}>
              {/* <Icon name="navigate" /> */}
              <Image source={require('./images/logo.png')} style={{ width: 30, height: 30, backgroundColor:'#ffd04d'  }} />
              <Text style={{ fontSize: 8 }}>Live</Text>
            </Button>
            <Button vertical onPress={this.ListSos}>
              <Icon name="help-buoy" style={{color:'#ffd04d'}} />
              
              <Text style={{ fontSize: 8 }}>SOS</Text>
            </Button>
            <Button vertical onPress={this.Logout}>
              <Icon name="person" style={{color:'#ffd04d'}}/>
              <Text style={{ fontSize: 8 }}>Logout</Text>
            </Button>
          </FooterTab>
        </Footer>
    );
  }
}

const styles = StyleSheet.create({
  newFooterBg: {
    color: 'red',
    backgroundColor: '#333333'
  }
});

module.export = AppFooter;
