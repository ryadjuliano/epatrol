import React, { Component } from 'react';
import { Text, Image, View } from 'react-native';
import { Card, ListItem, Icon, Header, Divider } from 'react-native-elements';


export default class AppHeader  extends Component {
    render() {
        return (
            <Header
            containerStyle={{
              backgroundColor: '#ffd04d',
              justifyContent: 'space-around',
              
            }}
            placement="left"
            // centerComponent={{ text: 'EPATROL', size:17, style: { color: '#000' } }}
            rightComponent={{ text: 'ECOGREEN OLEOCHEMICALS', color: '#fff' }}
            
            
            >
            <Image
                          source={require('./images/textlogo.png')}
                          style={{width:125, height:30 }}
                        /> 
             </Header>

        );
    }
}

module.export = AppHeader;


