import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Text, Icon, Right } from 'native-base';

export default class Schedule extends Component {
  render() {
    return (
      <Container>
        <Content>
          <Card>
          <CardItem header bordered>
            <Text>Jadwal Kegiatan</Text>
          </CardItem>
            <CardItem>
              <Icon active name="logo" />
              <Text>Kunjungan ke Anambas</Text>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
             </CardItem>
           </Card>
        </Content>
      </Container>
    );
  }
}
