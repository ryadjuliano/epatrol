import React, { Component } from 'react';
import { Image } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';


export default class News extends Component {
  render() {
    return (
      <Container>
        <Content>
          <Card>
            <CardItem>
              <Left>
                <Thumbnail source={{uri: 'https://samuelbonaparte.id/wp-content/uploads/2018/08/SamuelBonaparte.jpg'}} />
                <Body>
                  <Text>Ryad Juliano</Text>
                  <Text note>10 Desember 2018</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image source={{uri: 'https://samuelbonaparte.id/wp-content/uploads/2018/08/SamuelBonaparte.jpg'}} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            <CardItem>
             <Body>
               <Image source={{uri: 'Image URL'}} style={{height: 200, width: 200, flex: 1}}/>
               <Text>
                 Selamat  Bergabung.
               </Text>
             </Body>
           </CardItem>
           <CardItem>
             <Left>
               <Button transparent textStyle={{color: '#87838B'}}>
                 <Icon name="logo-github" />
                 <Text>1,926 stars</Text>
               </Button>
             </Left>
           </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}
