import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Alert,AsyncStorage, Dimensions,Image } from 'react-native';
import { Container, Left, Body, Right, Button, Title, Footer, FooterTab, Content, List, ListItem} from 'native-base';
import { Icon, Header,Input} from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import MapView, {
  Marker,
  AnimatedRegion,
  Polyline,
  PROVIDER_GOOGLE
} from "react-native-maps";
import MapViewDirections from 'react-native-maps-directions';
import Loader from 'react-native-modal-loader';  
import { GetMapsRoute, HeaderCounter, GetNewDataMap } from '../constants/services';
import {  updateTrackingLocation, GetScheduleTime, GuarOnlineTracking } from '../constants/services';
import AppFooter from '../../appfooter';
import AppHeader from '../../appheader';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;

const GOOGLE_MAPS_APIKEY = 'AIzaSyDOwIoYon9TVhqpFASALkEZfvSq7acI7gY';
const origin = { latitude: 1.0653160096281624, longitude: 104.13297845040131 };
const destination = { latitude: 1.06595962845581, longitude: 104.13065029297638, };


const LATITUDE = 1.065670;
const LONGITUDE = 104.132383;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = 0.0922;
//
// latitude: 1.0653160096281624,
// longitude: 104.13297845040131,
//
// latitude: 1.06595962845581,
// longitude: 104.13065029297638,


class LiveCheckQr extends Component {

  constructor(props) {
    super(props);

    // AirBnB's Office, and Apple Park
    this.state = {
      latitude: LATITUDE,
      longitude: LONGITUDE,
      routeCoordinates: [],
      distanceTravelled: 0,
      prevLatLng: {},
      dataTime: '',
      Koordinat: null,
      dataLogin: [],
      dataMaps: [],
      dataschedule:[],
      coordinate: new AnimatedRegion({
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: 0,
        longitudeDelta: 0
      })
    };
  

    this.mapView = null;
    
  
    state = {
      isLoading: false
    };
  }

  showLoader = () => {
    this.setState({ isLoading: true });
  };


  GetDataMaps () {
    const UserData = {
         kondisi : 'getRoutemaps',
       }

    // console.log(UserData)

    GetMapsRoute(UserData).then((result) => {
        console.log(result.res)
        this.setState({
          data:result.res
        })
      });
  }



  onMapPress = (e) => {
    this.setState({
      coordinates: [
        ...this.state.coordinates,
        e.nativeEvent.coordinate,
      ],
    });
  }

  
  openQr = async () => {
    Actions.qrcheck()
  }

  HeaderComponentCountData = () => {
    const params = {
      kondisi: 'CountHeader'
    }
    console.log(params)
    HeaderCounter(params).then((result) => {
      console.log(result.res)
      this.setState({
                incidents:result.res.data.jumlah_incident,
                sos:result.res.sos.jumlah_sos,
              })
      // this.setState({data:result.res})
    });

  }
  
  GUARDONS = async () => {

    // getGuardOnline
    const UserData = {
      kondisi: "getGuardOnline",
    }
    GuarOnlineTracking(UserData).then((result) => {
      console.log('onlineGuard  ON', result.res)

      this.setState({
        dataLogin: result.res
      })
    });

    const paramsMaps = {
      kondisi: 'getRoutemaps'
    }
    GetMapsRoute(paramsMaps).then((result) => {
      console.log('maps', result.res)

      this.setState({
        dataMaps: result.res
      })
    });


  }

  getMapRegion = () => ({
    latitude: this.state.latitude,
    longitude: this.state.longitude,
    latitudeDelta: LATITUDE_DELTA,
    longitudeDelta: LONGITUDE_DELTA
  });
  
  GetLoadNewData = async () => {
    const id = await AsyncStorage.getItem("idUser")
    const params = {
      kondisi: 'getDataNewMap',
      userid : id
    }
    // console.log(params)
    GetNewDataMap(params).then((result) => {
      console.log(result)
      // console.log(result.res[0])
      
      this.setState({ dataMap: result.res[0].point,dataschedule:result.res})
      // console.log('GetMAPNEXT',this.state.dataMap)
    }); 
  }


  componentDidMount() {
    this.showLoader()
    this.GetLoadNewData()
    this.HeaderComponentCountData()
    // this.USERGET()
    // this.GETTIMEPATROL()
    this.GUARDONS()
  }

  componentWillMount() {
    setTimeout(() => {
      this.setState({ 
        isLoading: false,
      })
    },
      3000)
  }

  render() {
    return (
      <Container>
      <AppHeader />

<View style={{ backgroundColor: 'black', width: '100%', height: 20, }}>

<View style={{ flex: 1, flexDirection: 'row' }}>
  <Text style={{ color: '#fff', marginLeft: 10, }}>Incident(s):{this.state.incidents}</Text>
  <Text style={{ color: '#fff', marginLeft: 10, }}>Cleared:0</Text>
  <Text style={{ color: '#fff', marginLeft: 150, }}>SOS:{this.state.sos}</Text>
</View>
</View>


<Loader loading={this.state.isLoading} color="#ff66be" />
          <MapView
            style={styles.map}
            provider={PROVIDER_GOOGLE}
            showUserLocation
            followUserLocation
            loadingEnabled
            region={this.getMapRegion()}
          >
            <Polyline coordinates={this.state.routeCoordinates} strokeWidth={5} />
            <Marker.Animated
              ref={marker => {
                this.marker = marker;
              }}
              coordinate={this.state.coordinate}
            />

            {/* { this.state.dataLogin.map()} */}

            {
              this.state.dataLogin.map((y) => {
                return (
                  <MapView.Marker
                    coordinate={{
                      latitude: Number(y.lati),
                      longitude: Number(y.longi),
                    }}
                  >
                    <Image
                      source={require('./../../images/guradon.png')}
                      style={{ width: 30, height: 50, }}
                    />
                  </MapView.Marker>
                )
              })
            }


            {
              this.state.dataMaps.map((x) => {
                return (
                  <MapView.Marker
                    coordinate={{
                      latitude: Number(x.lat),
                      longitude: Number(x.lng),
                    }}
                  >
                    {
                      x.status == 0 ?
                        <Image
                          source={require('./../../images/pin-red.png')}
                          style={{ width: 30, height: 50, }}
                        />
                        :
                        <Image
                          source={require('./../../images/pin.png')}
                          style={{ width: 30, height: 50, }}
                        />
                    }

                  </MapView.Marker>
                )
              })
            }


            <MapViewDirections
              origin={origin}
              destination={destination}
              apikey={GOOGLE_MAPS_APIKEY}
            />
          </MapView>


{/* edeef2 */}
<View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between', }}>
              


          <View style={{ width: 50, height: 50, backgroundColor: '' }} />
          <View style={{ width: 50, height: 50, backgroundColor: '' }} />
          <View style={{ width: "85%", height: 80, backgroundColor: '#edeef2', marginBottom: 20, marginLeft: 20, marginRight: 20, }} >
            <View style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between'

            }}>
              <View style={{ width: null, height: null, backgroundColor: 'transparent' }} >
              <Image
                      source={require('./../../images/camera.png')}
                      style={{ width: 50, height: 50, justifyContent:'center', marginLeft:20,marginTop:10,   }}
                    />
                

              </View>
             
              <View style={{ width: 50, height: 50, backgroundColor: 'transparent' }} >
                  <Text style={{ marginTop: 10, color: 'black', fontSize: 6,}}>
                      Name
                  </Text>
                  <Text style={{ marginTop: 0, color: 'black', fontSize: 10,fontWeight:'bold'}}>
                      -
                  </Text>
                  <Text style={{ marginTop: 10, color: 'black', fontSize: 6,}}>
                      Ref No
                  </Text>
                  <Text style={{ marginTop: 0, color: 'black', fontSize: 10, fontWeight:'bold'}}>
                      {this.state.dataMap}
                  </Text>
                </View>
                <View style={{ width: 50, height: 50, backgroundColor: 'transparent', marginRight:30,marginBottom:10}} >
                <TouchableOpacity >
                  
                  
                    <Image
                        source={require('../../images/calender.png')}  
                        style={{width:20, height:20,marginRight:100, }}
                      />
                      </TouchableOpacity>
                  <Text style={{ marginTop: 0, color: 'black', fontSize: 10,fontWeight:'bold'}}>
                      -
                  </Text>
                  <Text style={{ marginTop: 10, color: 'black', fontSize: 6,}}>
                      Status
                  </Text>
                  <Text style={{ marginTop: 0, color: 'black', fontSize: 10, fontWeight:'bold'}}>
                      -
                  </Text>
                   
                </View>

            </View>

          </View>
        </View>

     <AppFooter />
    </Container>
    );
  }
}

const styles = StyleSheet.create({
  gridView: {
    paddingTop: 25,
    paddingBottom:20,
    flex: 1,
    marginBottom:20,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 150,
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
  textInput: {
    marginTop:10,
    marginLeft:10,
    marginRight:10,
    width:null,
    height:40,
    borderColor: "#000",
    borderWidth: 0,
    borderRadius: 10,
    fontSize: 14,
    textAlign: "center",
    backgroundColor: "#edeef2",
    opacity: 1
  },
  textInputArea: {
    marginTop:10,
    marginLeft:10,
    marginRight:10,
    width:null,
    height:80,
    borderColor: "#000",
    borderWidth: 0,
    borderRadius: 10,
    fontSize: 14,
    textAlign: "center",
    backgroundColor: "#edeef2",
    opacity: 1
  },
  btnLogin: {
    marginTop:10,
    marginBottom: 30,
    marginLeft:10,
    width: 130,
    height:30,
    borderRadius:13,
    alignItems: 'center',
    backgroundColor: '#000'
  },
  btnText : {
    textAlign:'center',
    padding: 10,
    color: 'white',
    fontSize:10,
    fontWeight:'bold'
  },
  btnSend: {
    marginTop:10,
    marginBottom: 30,
    marginLeft:10,
    marginRight:10,
    width: null,
    height:50,
    borderRadius:13,
    alignItems: 'center',
    backgroundColor: '#000'
  },
  btnTexts : {
    textAlign:'center',
    padding: 15,
    color: 'white',
    fontSize:17,
    fontWeight:'bold'
  },
  containermp: {
   ...StyleSheet.absoluteFillObject,
   height: 800,
   width: 800,
   justifyContent: 'flex-end',
   alignItems: 'center',
   marginTop:100,
 },
 map: {
   ...StyleSheet.absoluteFillObject,
 },
});
export default LiveCheckQr;
