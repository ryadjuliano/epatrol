
import React from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Platform,
  PermissionsAndroid,
  Image,
  AsyncStorage
} from "react-native";
import { Container, Left, Body, Right, Button, Title, Footer, FooterTab, Content, List, ListItem } from 'native-base';
import MapView, {
  Marker,
  AnimatedRegion,
  Polyline,
  PROVIDER_GOOGLE
} from "react-native-maps";
import haversine from "haversine";
import MapViewDirections from 'react-native-maps-directions';
import { GetMapsRoute, HeaderCounter, GetNewDataMap, updateTrackingLocation, GetScheduleTime, GuarOnlineTracking } from '../constants/services';
import AppFooter from '../../appfooter';
import AppHeader from '../../appheader';
import Loader from 'react-native-modal-loader';
import { Actions } from 'react-native-router-flux';
// const LATITUDE = 1.065670;
// const LONGITUDE = 104.132383;
// const LATITUDE_DELTA = 0.0922;
// const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const GOOGLE_MAPS_APIKEY = 'AIzaSyDOwIoYon9TVhqpFASALkEZfvSq7acI7gY';
const origin = { 
          latitude: 1.065348190572742, longitude: 104.13302673016358,
        

          
         };
const destination = {  latitude: 1.065348190572742, longitude: 104.13302673016358,
  latitude: 1.0650049271466004, longitude: 104.1309238782959,
  latitude: 1.0658309047010281, longitude: 104.1309238782959,
  latitude: 1.066646155057007, longitude: 104.13041425858307,
  latitude: 1.0675257670409704, longitude: 104.13118137036133, };


const LATITUDE = 1.065670;
const LONGITUDE = 104.132383;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = 0.0922;
// const LATITUDE = 37.78825;
// const LONGITUDE = -122.4324;

// const LATITUDE = 1.065670;
// const LONGITUDE = 104.132383;
// const LATITUDE_DELTA = 0.0922;
// const LONGITUDE_DELTA = 0.0922;
// const LATITUDE_DELTA = 0.0922;
// const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

// const GOOGLE_MAPS_APIKEY = 'AIzaSyDOwIoYon9TVhqpFASALkEZfvSq7acI7gY';
// const origin = {latitude: 1.0653160096281624, longitude: 104.13297845040131};
// const destination = {latitude: 1.06595962845581, longitude: 104.13065029297638, };



class LiveTracking extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      latitude: LATITUDE,
      longitude: LONGITUDE,
      routeCoordinates: [],
      distanceTravelled: 0,
      prevLatLng: {},
      dataTime: '',
      Koordinat: null,
      dataLogin: [],
      dataMaps: [],
      coordinate: new AnimatedRegion({
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: 0,
        longitudeDelta: 0
      })
    };
  }

  openQr = async () => {
    Actions.qrcheck()
  }

  GETTIMEPATROL = async () => {

    const UserData = {

      kondisi: 'getTimeSchedule',
      userid:await AsyncStorage.getItem("idUser"),

    }

    // console.log(UserData)

    GetScheduleTime(UserData).then((result) => {
      console.log(result.res[0].time_start)

      this.setState({
        dataTime: result.res[0].time_start,
        alur: result.res[0].alur,
        // dataTime: result.res[0].time_start
      })
    });
  }

  GUARDONS = async () => {

    // getGuardOnline
    const UserData = {
      kondisi: "getGuardOnline",
    }
    GuarOnlineTracking(UserData).then((result) => {
      console.log('onlineGuard  ON', result.res)

      this.setState({
        dataLogin: result.res
      })
    });
 
    const paramsMaps = {
      kondisi: 'getRoutemaps',
      idUser : await AsyncStorage.getItem("idUser")
    }  
    GetMapsRoute(paramsMaps).then((result) => {
      console.log('maps', result.res)

      this.setState({
        dataMaps: result.res
      })
    });


  }
  USERGET = async () => {
    const userid = await AsyncStorage.getItem("idUser")
    this.setState({
      userID: userid
    })
  }
  componentDidMount() {
    this.USERGET()
    this.GETTIMEPATROL()
    this.GUARDONS()
    const { coordinate } = this.state;

    this.requestCameraPermission();

    this.watchID = navigator.geolocation.watchPosition(
      position => {
        const { routeCoordinates, distanceTravelled } = this.state;
        const { latitude, longitude } = position.coords;

        const newCoordinate = {
          latitude,
          longitude
        };
        console.log('tolong', { newCoordinate });

        if (Platform.OS === "android") {
          if (this.marker) {
            this.marker._component.animateMarkerToCoordinate(
              newCoordinate,
              500
            );

            // 
            const UserData = {
              idUser: this.state.userID,
              kondisi: 'setLocationTracking',
              lati: this.state.latitude,
              longi: this.state.longitude,
            }

            // console.log(UserData)

            updateTrackingLocation(UserData).then((result) => {
              console.log(result)
              // this.setState({
              //   data:result.res
              // })
            });
          }
        } else {
          coordinate.timing(newCoordinate).start();
        }

        this.setState({
          latitude,
          longitude,
          routeCoordinates: routeCoordinates.concat([newCoordinate]),
          distanceTravelled:
            distanceTravelled + this.calcDistance(newCoordinate),
          prevLatLng: newCoordinate
        });
      },
      // error => console.log(error),
      // {
      //   enableHighAccuracy: true,
      //     timeout: 2000,
      //   maximumAge: 100,
      //   distanceFilter: 10
      // }
    );
  }
  refreshLink() {
    Actions.livetracking()
  }
  
  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
  }

  getMapRegion = () => ({
    latitude: this.state.latitude,
    longitude: this.state.longitude,
    latitudeDelta: LATITUDE_DELTA,
    longitudeDelta: LONGITUDE_DELTA
  });

  calcDistance = newLatLng => {
    const { prevLatLng } = this.state;
    return haversine(prevLatLng, newLatLng) || 0;
  };

  requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "Location Access Permission",
          buttonNeutral: "Ask Me Later",
          buttonNegative: "Cancel",
          buttonPositive: "OK"
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the camera");
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      // console.warn(err);
    }
  };

  render() {
    return (
      <Container>
        <AppHeader />


        <View style={styles.container}>


          <View style={{ backgroundColor: 'black', width: '100%', height: 20, }}>

            <View style={{ flex: 1, flexDirection: 'row' }}>
              <Text style={{ color: '#fff', marginLeft: 10, }}>Incident(s):{this.state.incidents}</Text>
              <Text style={{ color: '#fff', marginLeft: 10, }}>Cleared:0</Text>
              <Text style={{ color: '#fff', marginLeft: 150, }}>SOS:{this.state.sos}</Text>
            </View>
          </View>

                   

          <Loader loading={this.state.isLoading} color="#ff66be" />

          
          <MapView
            style={styles.map}
            provider={PROVIDER_GOOGLE}
            showUserLocation
            followUserLocation
            loadingEnabled
            region={this.getMapRegion()}
          >


            <Polyline coordinates={this.state.routeCoordinates} strokeWidth={5} />
            <Marker.Animated
              ref={marker => {
                this.marker = marker;
              }}
              coordinate={this.state.coordinate}
            />

            {/* { this.state.dataLogin.map()} */}

            {
              this.state.dataLogin.map((y) => {
                return (
                  <MapView.Marker
                    coordinate={{
                      latitude: Number(y.lati),
                      longitude: Number(y.longi),
                      
                    }}
                    title={y.nama}
                    description={y.posisi}
                    
                  >
                    <Image
                      source={require('./../../images/guradon.png')}
                      style={{ width: 30, height: 50, }}
                    />
                  </MapView.Marker>
                )
              })
            }


            {
              this.state.dataMaps.map((x,i) => {
                return (
                  <MapView.Marker
                    coordinate={{
                      latitude: Number(x.lat),
                      longitude: Number(x.lng),
                    }}
                    title={x.nama}
                    description={x.posisi}
                    key={i}
                  >
                    {
                      x.status == 0 ?
                        <Image
                          source={require('./../../images/pin-red.png')}
                          style={{ width: 30, height: 50, position:'absolute' }}
                        />
                        :
                        // x.status == 2 ?
                        <Image
                        source={require('./../../images/pin.png')}
                        style={{ width: 30, height: 50,position:'absolute' }}
                      />
                      //   :
                        
                      //   <Image
                      //   source={require('./../../images/pin-yellow.jpg')}
                      //   style={{ width: 30, height: 50,position:'absolute' }}
                      // />
                    }
                    
                  </MapView.Marker>
                  
                )
              })
            }

            {
              this.state.dataMaps.map((x) => {
                return (
                  <MapViewDirections
                  origin={origin}
                  destination={{
                    latitude: Number(x.lat),
                    longitude: Number(x.lng),
                  }}
                  apikey={GOOGLE_MAPS_APIKEY}
                  strokeWidth={0}
                  strokeColor="white"
                  optimizeWaypoints={true}
                />
                )
              })}
              

            {/* <MapView.Marker
        
        coordinate={{
          latitude: 1.0653160096281624,
          longitude: 104.13297845040131,

        }}   
        > 
           <Image
              source={require('./../../images/pin.png')}
              style={{width:30,height:50,}}
            />
          </MapView.Marker>

      
      <MapView.Marker
          coordinate={{
            latitude: 1.0651028108618505,
            longitude: 104.13249833498764,
          }} />

      
        <MapView.Marker
          coordinate={{
            latitude: 1.0648896120808062,
            longitude: 104.13084341202546,

          }} />
        
        <MapView.Marker
            coordinate={{
              latitude: 1.06595962845581,
              longitude: 104.13065029297638,

            }} />
        
        
        <MapView.Marker
            coordinate={{
              latitude: 1.0669894183001902,
              longitude: 104.13030160580445,

            }} /> */}

           
          </MapView>
          <View style={styles.buttonContainer}>
            {/* <TouchableOpacity style={[styles.bubble, styles.button]}>
            <Text style={styles.bottomBarContent}>
              {parseFloat(this.state.distanceTravelled).toFixed(2)} km
            </Text>
          </TouchableOpacity> */}

            <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between', }}>


              <View style={{ width: 50, height: 50, backgroundColor: '' }} />
              <View style={{ width: 50, height: 50, backgroundColor: '' }} />
              <View style={{ width: "85%", height: 80, backgroundColor: '#edeef2', marginBottom: 20, marginLeft: 20, marginRight: 20, }} >
                <View style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'space-between'

                }}>
                  <View style={{ width: null, height: null, backgroundColor: 'transparent' }} >
                    <Text style={{ marginTop: 10, color: 'black', fontSize: 10, marginLeft: 50 }}>
                      Next Schedule start in
                    </Text>
                    <Text style={{ marginLeft: 30, fontSize: 36, }}>
                      {this.state.dataTime}
                    </Text>


                  </View>
                  <View style={{ width: 50, height: 50, backgroundColor: 'transparent' }} >
                    <Text style={{ marginTop: 10, color: 'black', fontSize: 6, }}>
                      Patrol Type
                      </Text>
                    <Text style={{ marginTop: 0, color: 'black', fontSize: 10, fontWeight: 'bold' }}>
                      {this.state.alur}
                      </Text>
                    <Text style={{ marginTop: 10, color: 'black', fontSize: 6, }}>
                      Starting Point
                      </Text>
                    <Text style={{ marginTop: 0, color: 'black', fontSize: 10, fontWeight: 'bold' }}>
                      {this.state.dataMap}
                    </Text>
                  </View>
                  <View style={{ width: 50, height: 50, backgroundColor: 'transparent', marginRight: 30, marginBottom: 10 }} >
                    {/* <Text style={{ marginTop: 10, color: 'black', fontSize: 6,}}>
                          Total Distance
                      </Text>
                      <Text style={{ marginTop: 0, color: 'black', fontSize: 10,fontWeight:'bold'}}>
                          1000m
                      </Text>
                      <Text style={{ marginTop: 10, color: 'black', fontSize: 6,}}>
                          Estimated Time
                      </Text>
                      <Text style={{ marginTop: 0, color: 'black', fontSize: 10, fontWeight:'bold'}}>
                          50min
                      </Text> */}
                    <TouchableOpacity onPress={this.openQr}>
                      <Image
                        source={require('../../images/qrcode.png')}
                        style={{ width: 40, height: 40, marginRight: 100, }}
                      />
                    </TouchableOpacity>


                    <TouchableOpacity onPress={this.refreshLink}>
                      <Image
                        source={require('../../images/refresh.png')}
                        style={{ width: 40, height: 40, marginRight: 100, }}
                      />
                    </TouchableOpacity>

                   
                  </View>

                </View>

              </View>
            </View>

          </View>
          <AppFooter />
        </View>

      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: "flex-end",
    alignItems: "center"
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  bubble: {
    flex: 1,
    backgroundColor: "rgba(255,255,255,0.7)",
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20
  },
  latlng: {
    width: 200,
    alignItems: "stretch"
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: "center",
    marginHorizontal: 10
  },
  buttonContainer: {
    flexDirection: "row",
    marginVertical: 20,
    backgroundColor: "transparent"
  }
});

export default LiveTracking;