import React, { Component } from 'react';
import { AsyncStorage } from 'react-native'
import { Header, Container, Left, Right, Body, Icon, Button, Title } from 'native-base'

import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
} from 'react-native';

import QRCodeScanner from 'react-native-qrcode-scanner';
import { Actions } from 'react-native-router-flux';
import Loader from 'react-native-modal-loader';
import { CheckInPoint }  from '../constants/services'

var moment = require('moment');


export default class ScanQr extends Component {

  state = {
    isLoading : false
  }
 
  onSuccess = async (e) => {
    // const details = {
    //   'token': e.data,
    //   'barcode' : e.data
    // }
    // alert(e.data)

    const id = await AsyncStorage.getItem("idUser")
    const pola = await AsyncStorage.getItem("pola")
    const startingTime = moment().format('LT');

    this.setState({ isLoading: true});
    const details = {
      'userid' : id,
      'startingTime' : startingTime,
      'patrolType' : 'PATTERN',
      'point' : e.data,
      'lat' : '111',
      'lang' : '1110',
      'time' : startingTime,
      'kondisi' : 'CheckIn',
      'pola': pola,

    }
    // alert(e.data)

    console.log(details)
    // alert(details);
    CheckInPoint(details).then((result) => {
      console.log(result)
      if (result.res.status == 1) {
        alert(result.res.message)
        Actions.mainmenu()
        // Actions.livetracking()
        this.setState({ isLoading: false});
      } else {
        alert(result.res.message)
        this.setState({ isLoading: false });
        // Actions.livetracking()
        Actions.mainmenu()
      }
    });

  }

  mainmenu() {
    Actions.menupanitia()
  }

  render() {
    return (

      <Container>
            <Header style={{ backgroundColor: '#ffd04d' }}>
                    <Left>
                        <Button transparent onPress={() => Actions.pop()}>
                            <Icon name='arrow-back'  style={{ color: '#000' }}/>
                        </Button>
                    </Left>
                    <Body>
                        <Title styl={{color:'#fff'}}>Scan QR</Title>
                    </Body>
                    <Right>
                        <Button transparent>
                            {/* <Icon name='menu' /> */}
                        </Button>
                    </Right>
                </Header>
                <Loader loading={this.state.isLoading} color="#ffd04d" />
      <QRCodeScanner
        onRead={this.onSuccess.bind(this)}
        topContent={
          <Text style={styles.centerText}>
            Welcome <Text style={styles.textBold}>EPATROL</Text> on your Device and scan the QR code.
          </Text>
        }
        bottomContent={
          <TouchableOpacity style={styles.buttonTouchable} >
            <Text style={styles.buttonText}>OK. Got it!</Text>
          </TouchableOpacity>
        }
      />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
  },
});
