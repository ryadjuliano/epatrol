import React, { Component } from 'react';
import { View, Text, TextInput,BackHandler, TouchableOpacity, Alert, ImageBackground, Image, StyleSheet, StatusBar, TouchableHighlight, AlertIOS, AsyncStorage, ActivityIndicator } from 'react-native';
import { Form, Item, stackedLabel, Input, Label, Button, Content, TabHeading, Container, Header } from 'native-base';
import { PostData } from '../constants/services';
// import Telephony from 'react-native-telephony-apis';

import Loader from 'react-native-modal-loader';
import { Actions } from 'react-native-router-flux';
import PasswordInputText from 'react-native-hide-show-password-input';
import { TextField } from 'react-native-material-textfield';

// import datalogin from '../bankdata/datalogin'
export default class Login extends Component {

  constructor(props) {
    super(props)
    // this.datalogin = new datalogin();
    // this.datalogin.membershipID = this.state.membershipID,
    // this.datalogin.membershipPIN = 'Ryad'
    this.state = {
      membershipID: '',
      membershipPIN: '',
    }
  }

  showLoader = () => {
    this.setState({ isLoading: true });
  };

  
  state = {
    isLoading: false
  };

  signup() {
    Actions.signup()
  }

  UserLoginFunction = () => {
    this.setState({ isLoading: true });
    const { membershipID, membershipPIN } = this.state
    if (membershipID && membershipPIN === null) {
      alert('Please complete your field')
    }
    else {
      const params = {
        username: membershipID,
        password: membershipPIN,
        kondisi: 'login'
      }
      console.log(params)
      PostData(params).then((result) => {
        console.log(result)
        if (result.users.status == 0) {
          alert('Please check username or password')
          this.setState({ isLoading: false });
        } else {
          this.setState({ isLoading: false });
          const userId = result.users.data.id
          // alert(roleaccess)
          AsyncStorage.setItem("idUser", userId)
          AsyncStorage.setItem("statusUser", "1")
          AsyncStorage.setItem("roleaccess", result.users.data.role)
          AsyncStorage.setItem("pola", result.users.data.pola)
          Actions.mainmenu()

        }
        // this.setState({
        //   data:result.res
        // })
      });
    }
  }

  CekLogin = async () => {
    try {
      const id = await AsyncStorage.getItem("statusUser");
      // this.setState({ isLoading: true });
      // alert(id);
      if (id !== null) {
        Actions.mainmenu()
      }
    } catch (error) {
      // Error retrieving data
    }
  }

  fogotPassword() {
    // Actions.forgot()
    alert('Please contact your Administrator')
  }

  componentDidMount() {
    this.CekLogin()
  }
  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
      BackHandler.exitApp();
    }

  render() {
    return (
      <View style={{ backgroundColor: '#ffd04d', flex: 1 }}>

        <View style={{
          justifyContent: 'center',
          alignItems: 'center',
        }}>
          <Image source={require('./../../images/logoguardstaffs.png')} style={{ width: 200, height: 200, }} />
        </View>   
        {/* <ImageBackground source={require('./../../images/logoguardstaff.png')} style={{
          flex: 1,logohsestaff  logohsechief 
          resizeMode:'cover',
        }} > */}
        {/* <View style={{flex:1,top:0, justifyContent:'space-between'}}>
        <Image source={require('./../../images/logoguardstaff.png')} style={{ flex:1,width:200, height:200, }} />

        </View> */}
        <View style={{ marginTop: 0, bottom: 60, }}>
          <Form style={{
            backgroundColor: 'transparent', margin: 15, borderRadius: 13, paddingLeft: 20, paddingRight: 20, paddingTop: 30, backgroundColor: 'rgba(255,255,255,0.9)'
          }} >
            {/* <Item stackedLabel style={{ marginLeft: 0, }}>
              <Label style={styles.stack}>Username</Label>
              <Input onChangeText={membershipID => this.setState({ membershipID })} />
            </Item> */}
            <View style={{margin: 0}}>
                {/* <InputText
                    value={this.state.password}
                    onChangeText={ (password) => this.setState({ password }) }
                /> */}
                 <TextField
                label='Username'
                value={this.state.membershipID}
                onChangeText={membershipID => this.setState({ membershipID })}
      />
            </View>
            <View style={{margin: 0}}>
                <PasswordInputText
                    onChangeText={(text) => this.setState({ membershipPIN: text })}
                    value={this.state.membershipPIN }
                />
            </View>
            {/* <Item stackedLabel style={{ margin: 20, }} >
              <Label style={styles.stack}>Password</Label>
              <PasswordInputText onChangeText={(text) => this.setState({ membershipPIN: text })}
                value={this.state.membershipPIN}
                secureTextEntry={true} />
            </Item> */}
            
            <Loader loading={this.state.isLoading} color="#ff66be" />
            <TouchableOpacity onPress={this.fogotPassword}><Label style={styles.LabelFogotPassword}> Forget Your Password?</Label></TouchableOpacity>

            <View style={{ position: 'relative', height: 60, marginTop: 10, marginBottom: 25, }}>
              <Button light style={styles.buttonContainer} onPress={this.UserLoginFunction} >
                <Text style={styles.buttonText}>SIGN IN</Text>
              </Button>
            </View>

            <View style={{
              borderBottomColor: '#DEDEDE',
              borderBottomWidth: 1.0,
              // paddingLeft:5,
              // margin:-5,
            }}>
            </View>

            <View style={{

              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'transparent',
              marginBottom: 50
            }}>
              <Loader loading={this.state.isLoading} color="#ffd04d" />

            </View>


          </Form>
          <View style={[styles.containerss, styles.horizontal]}>

          </View>

        </View>

        {/* </ImageBackground> */}
      </View>
    );


  }

};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderRadius: 25,
    margin: 15
  },

  signupTextCont: {
    flexGrow: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingVertical: 16,
    flexDirection: 'row',

  },
  LabelFogotPassword: {
    position: 'relative',
    paddingTop: 10,
    paddingLeft: 10,
    fontSize: 14,
    color: '#000000',
    marginTop: 5,
  },
  buttonContainer: {
    backgroundColor: '#000',
    borderRadius: 22,
    bottom: 0,
    position: 'absolute',
    right: 0,



  },
  buttonText: {

    color: '#fff',
    fontWeight: 'bold',
    fontSize: 17,
    paddingLeft: 25,
    paddingRight: 25,


  },
  forgotText: {
    margin: 10,
    backgroundColor: 'white',

  },
  TextInputStyleClass: {
    textAlign: 'center',
    marginTop: 100,
    height: 40,
    borderWidth: 1,
    // Set border Hex Color Code Here.
    borderColor: '#2196F3',

    // Set border Radius.
    borderRadius: 5,

    // Set border Radius.
    //borderRadius: 10 ,
  },
  stack: {
    padding: 2,
    fontSize: 10,
    fontWeight: 'bold',
    color: '#8b8b8b'

  },



});
