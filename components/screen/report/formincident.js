import React, { Component } from 'react';
import { StyleSheet, Image, View, Text, TouchableOpacity, Alert,AsyncStorage, TextInput } from 'react-native';
import { Container, Left, Body, Right, Button, Title, Footer, FooterTab, Content, List, ListItem } from 'native-base';
import { Icon, Header, Input } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import AppFooter from '../../appfooter';
import { IncidentStore, HeaderCounter } from '../constants/services'
import AppHeader from '../../appheader';
import ImagePicker from 'react-native-image-picker';
// import Icon from 'react-native-vector-icons'


export default class FormIncident extends Component {

  constructor(props) {
    super(props);
    // console.log('',this.props.kodeMobil)
    this.state = {
      lokasi: '',
      kategori: '',
      deskripsi: '',
      path: this.props.path,
      metaimage: this.props.metaimage,
      incidents:null,
      sos:null
    }
  }

  btnSubmit = () => {
    // const {location,categoruy,description} = this.state
    // const params = {
    //
    // }
    console.log('formincident', this.state.metaimage.fileName)
  }

  uploadPicture = async () => {

    

    let r = Math.random().toString(36).substring(7);
    const id = await AsyncStorage.getItem("idUser")
    const Filename =  this.state.GetDataImage.fileName
    console.log(Filename.fileName)
    const params = {
      userid: id,
      lokasi: this.state.lokasi,
      deskripsi: this.state.deskripsi,
      image: Filename,
      kategori: this.props.category,
      kondisi: 'incident'
    }

    console.log(params)
    
      console.log("random", r+'.jpg');
    IncidentStore(params).then((result) => {
            
            console.log(result)
            console.log('mulai upload');
            this.setState({ loading: true })
            const data = new FormData();
            data.append('userid', id);
            data.append('fileToUpload', {
              uri: this.state.path,
              type: 'image/jpeg',
              name:  r+'.jpg',
              
            });

            const url = "http://165.22.63.165:2000/android/uploads.php"
            fetch(url, { 
              method: 'POST',
              body: data
            })
              .then((response) => response.json())
              .then((responseJson) => {
                console.log('ini dari upload',responseJson);
              
                if (responseJson.status == 1) {
                  // alert('Succes')
                  Actions.mainmenu()
                } else {
                  alert('Image cannot be upload');
                }
                this.setState({
                  loading: false
                })
              });
    });

    // console.log(params)


  }

  

  HeaderComponentCountData = () => {
    const params = {
      kondisi: 'CountHeader'
    }
    console.log(params)
    HeaderCounter(params).then((result) => {
      console.log(result.res)
      this.setState({
                incidents:result.res.data.jumlah_incident,
                sos:result.res.sos.jumlah_sos,
              })
      // this.setState({data:result.res})
    });

  }

  componentDidMount() {
    this.HeaderComponentCountData()
  }

  openCamera = () => {
    // Actions.camera({category:this.props.category})
    const options = { quality: 0.5, base64: true, mirrorImage: true };
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
     
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { path: response.uri };
        console.log(source)
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
     
        this.setState({
          path: response.uri,
          GetDataImage : response,
        });
      }
    });
  }


  render() {

    return (
      <Container>
       <AppHeader />


       
<View style={{ backgroundColor: 'black', width: '100%', height: 20, }}>

<View style={{ flex: 1, flexDirection: 'row' }}>
  <Text style={{ color: '#fff', marginLeft: 10, }}>Incident(s):{this.state.incidents}</Text>
  <Text style={{ color: '#fff', marginLeft: 10, }}>Cleared:0</Text>
  <Text style={{ color: '#fff', marginLeft: 150, }}>SOS:{this.state.sos}</Text>
</View>
</View>

        <Content>
          <View style={{ backgroundColor: '#ffd04d', width: 350, height: 20, marginTop: 10, marginRight: 10, marginLeft: 10 }}>

            {this.props.category === 1 ? <Text>Merorkok</Text> :
              this.props.category === 2 ? <Text>Kecelakan Kerja</Text> :
                this.props.category === 3 ? <Text>Kerusakan Fasilitas</Text> :
                  this.props.category === 4 ? <Text>Kebakaran</Text> :
                    this.props.category === 5 ? <Text>Perkelahian</Text> :
                      this.props.category === 6 ? <Text>Pencurian</Text> :
                        this.props.category === 7 ? <Text>Lainnya</Text> :
                          null}
          </View>

          <TextInput style={styles.textInput} onChangeText={lokasi => this.setState({ lokasi })} placeholder="Add Location" />
          <TextInput style={styles.textInputArea} onChangeText={deskripsi => this.setState({ deskripsi })} placeholder="Add Description" />


          {/* <TouchableOpacity onPress={this.openCamera} underlayColor="white"> */}
            <View style={styles.btnLogin}>
            

            
            <View style={{flex: 1, flexDirection: 'row',marginTop:20,}}>
            
              <View style={{width: 50, height: 50, backgroundColor: 'transparent', marginRight:50,}}>
              <TouchableOpacity onPress={this.openCamera} underlayColor="white">
                  <Image
                    style={{ width: 50, height: 50 }}
                    source={require('../../images/camera.png')}
                  />
                </TouchableOpacity>    
              </View>
              
              {/* <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} /> */}
              <View style={{width: 50, height: 50, backgroundColor: 'transparent', marginLeft:50,}} >
              <TouchableOpacity onPress={this.openCamera} underlayColor="white">
                <Image
                    style={{ width: 50, height: 50 }}
                    source={require('../../images/camera.png')}
                  />
                </TouchableOpacity>    
                </View>
                
            </View>

              <Image
                source={{ uri: this.state.path }}
                style={{ width: 200, height: 70, marginTop:70, }}
              />

            </View>
          {/* </TouchableOpacity> */}


          <TouchableOpacity onPress={this.uploadPicture} underlayColor="white">
            <View style={styles.btnSend}>
              <Text style={styles.btnTexts}>SEND</Text>
            </View>
          </TouchableOpacity>

        </Content>
        <AppFooter />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  gridView: {
    paddingTop: 25,
    paddingBottom: 20,
    flex: 1,
    marginBottom: 20,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 150,
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
  textInput: {
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    width: null,
    height: 40,
    borderColor: "#000",
    borderWidth: 0,
    borderRadius: 10,
    fontSize: 14,
    textAlign: "center",
    backgroundColor: "#edeef2",
    opacity: 1
  },
  textInputArea: {
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    width: null,
    height: 80,
    borderColor: "#000",
    borderWidth: 0,
    borderRadius: 10,
    fontSize: 14,
    textAlign: "center",
    backgroundColor: "#edeef2",
    opacity: 1
  },
  btnLogin: {
    marginTop: 10,
    marginBottom: 30,
    marginLeft: 10,
    width: null,
    height: 100,
    borderRadius: 13,
    alignItems: 'center',

    justifyContent: 'center'
  },
  btnText: {
    textAlign: 'center',
    padding: 10,
    color: 'white',
    fontSize: 10,
    fontWeight: 'bold'
  },
  btnSend: {
    marginTop: 10,
    marginBottom: 30,
    marginLeft: 10,
    marginRight: 10,
    width: null,
    height: 50,
    borderRadius: 13,
    alignItems: 'center',
    backgroundColor: '#000'
  },
  btnTexts: {
    textAlign: 'center',
    padding: 15,
    color: 'white',
    fontSize: 17,
    fontWeight: 'bold'
  },
});
