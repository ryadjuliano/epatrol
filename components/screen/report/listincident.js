import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Alert } from 'react-native';
import { Container, Left, Body, Right,Icon ,Button, Title, Footer, FooterTab, Content, List, ListItem } from 'native-base';
import {  Header } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import AppFooter from '../../appfooter';
import { HeaderCounter, IncidentQueryList } from './../constants/services'
import AppHeader from '../../appheader';
// import Icon from 'react-native-vector-icons'


export default class ListIncident extends Component {

  constructor(props) {
    super(props)
    this.state = {
      data: [],
      incidents:null,
      sos:null,
      dataincident:[],
    }
  }
  
  tabAction (params){
    Actions.reportform({category:params})
  }


  HeaderComponentCountData = () => {
    const params = {
      kondisi: 'CountHeader'
    }
    console.log(params)
    HeaderCounter(params).then((result) => {
      console.log(result.res)
      this.setState({
                incidents:result.res.data.jumlah_incident,
                sos:result.res.sos.jumlah_sos,
              })
      // this.setState({data:result.res})
    });

  }

  IncidentList = () => {
    const params = {
      kondisi: 'getIncidentList'
    }
    console.log(params)
    IncidentQueryList(params).then((result) => {
      console.log(result.res)
      this.setState({
                dataincident:result.res,
              })
      // this.setState({data:result.res})
    });

  }


  componentDidMount() {

    this.HeaderComponentCountData()
    this.IncidentList()

  }

  render() {
    const list = [
      {
        title: 'Appointments',
        icon: 'av-timer'

      },
      {
        title: 'Trips',
        icon: 'flight-takeoff'
      },
    ];

    return (
      <Container>
     <AppHeader />


        
<View style={{ backgroundColor: 'black', width: '100%', height: 20, }}>

<View style={{ flex: 1, flexDirection: 'row' }}>
  <Text style={{ color: '#fff', marginLeft: 10, }}>Incident(s):{this.state.incidents}</Text>
  <Text style={{ color: '#fff', marginLeft: 10, }}>Cleared:0</Text>
  <Text style={{ color: '#fff', marginLeft: 150, }}>SOS:{this.state.sos}</Text>
</View>
</View>

        <Content>
        {
          this.state.dataincident.map((y) => {

            return(
              <ListItem icon onPress={() => {this.tabAction(y.id_inc)}}>
              <Left>
                <Button style={{ backgroundColor: "grey" }}>
                  <Icon active name="help-buoy" />
                </Button>
              </Left>
              <Body>
                <Text>{y.inc_name}</Text>
              </Body>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
            )
          })
        }
       

          {/* <ListItem icon onPress={() => {this.tabAction(2)}}>
              <Left>
                <Button style={{ backgroundColor: "grey" }}>
                  <Icon active name="medkit" />
                </Button>
              </Left>
              <Body>
                <Text>Kecelakan Kerja</Text>
              </Body>
              <Right>
                <Icon name="arrow-dropright" />
              </Right>
            </ListItem>

            <ListItem icon onPress={() => {this.tabAction(3)}}>
                <Left>
                  <Button style={{ backgroundColor: "grey" }}>
                    <Icon active name="film" />
                  </Button>
                </Left>
                <Body>
                  <Text>Kerusakan Fasilitas</Text>
                </Body>
                <Right>
                  <Icon name="arrow-dropright" /> 
                </Right>
              </ListItem>
              <ListItem icon onPress={() => {this.tabAction(4)}}>
                  <Left>
                    <Button style={{ backgroundColor: "grey" }}>
                      <Icon active name="bonfire" />
                    </Button>
                  </Left>
                  <Body>
                    <Text>Kebakaran</Text>
                  </Body>
                  <Right>
                    <Icon name="arrow-dropright" />
                  </Right>
                </ListItem>
                <ListItem icon onPress={() => {this.tabAction(5)}}>
                    <Left>
                      <Button style={{ backgroundColor: "grey" }}>
                        <Icon active name="contacts" />
                      </Button>
                    </Left>
                    <Body>
                      <Text>Perkelahian</Text>
                    </Body>
                    <Right>
                      <Icon name="arrow-dropright" />
                    </Right>
                  </ListItem>
                  <ListItem icon onPress={() => {this.tabAction(6)}}>
                      <Left>
                        <Button style={{ backgroundColor: "grey" }}>
                          <Icon  name="close-circle" />
                        </Button>
                      </Left>
                      <Body>
                        <Text>Pencurian</Text>
                      </Body>
                      <Right>
                        <Icon name="arrow-dropright" />
                      </Right>
                    </ListItem>
                    <ListItem icon onPress={() => {this.tabAction(7)}}>
                        <Left>
                          <Button style={{ backgroundColor: "grey" }}>
                            <Icon active name="more" />
                          </Button>
                        </Left>
                        <Body>
                          <Text>Lainnya</Text>
                        </Body>
                        <Right>
                          <Icon name="arrow-dropright" />
                        </Right>
                      </ListItem> */}

         </Content>

    <AppFooter />
    </Container>
    );
  }
}

const styles = StyleSheet.create({
  gridView: {
    paddingTop: 25,
    paddingBottom:20,
    flex: 1,
    marginBottom:20,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 150,
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
});
