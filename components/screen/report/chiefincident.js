import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Alert } from 'react-native';
import { Container, Left, Body, Right,Icon ,Button, Title, Footer, FooterTab, Content, List, ListItem } from 'native-base';
import {  Header } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import AppFooter from '../../appfooter';
import { HeaderCounter,  GetListIncidentGuard } from './../constants/services'
import AppHeader from '../../appheader';
// import Icon from 'react-native-vector-icons'
import Loader from 'react-native-modal-loader';
var moment = require('moment');
export default class Chiefincident extends Component {

  constructor(props) {
    super(props)
    this.state = {
      data: [],
      incidents:null,
      sos:null
    }
    state = {
        isLoading : false
    }
  }
  
  showLoader = () => {
    this.setState({ isLoading: true });
  };
  
  tabAction (params){
    Actions.reportform({category:params})
  }


  HeaderComponentCountData = () => {
    const params = {
      kondisi: 'CountHeader'
    }
    console.log(params)
    HeaderCounter(params).then((result) => {
      console.log(result.res)
      this.setState({
                incidents:result.res.data.jumlah_incident,
                sos:result.res.sos.jumlah_sos,
              })
      // this.setState({data:result.res})
    });

  }

  IncidentList = () => {
    const params = {
      kondisi: 'getDashboard'
    }
    console.log(params)
    GetListIncidentGuard(params).then((result) => {
      console.log(result)
      if (result.res === null) {
        // alert('No Data')
        this.setState({ isLoading: false});
      } else {
        this.setState({ isLoading: false, data:result.res });
      }
      // this.setState({})
    });

  }

  componentDidMount() {
    this.setState({ isLoading: true });
    this.HeaderComponentCountData()
    this.IncidentList()

  }

  render() {
    const list = [
      {
        title: 'Appointments',
        icon: 'av-timer'

      },
      {
        title: 'Trips',
        icon: 'flight-takeoff'
      },
    ];

    return (
      <Container>
     <AppHeader />


        
<View style={{ backgroundColor: 'black', width: '100%', height: 20, }}>

<View style={{ flex: 1, flexDirection: 'row' }}>
  <Text style={{ color: '#fff', marginLeft: 10, }}>Incident(s):{this.state.incidents}</Text>
  <Text style={{ color: '#fff', marginLeft: 10, }}>Cleared:0</Text>
  <Text style={{ color: '#fff', marginLeft: 150, }}>SOS:{this.state.sos}</Text>
</View>
</View>

        <Content>

        {/* <ListItem icon onPress={() => {this.tabAction(1)}}>
            <Left>
              <Button style={{ backgroundColor: "grey" }}>
                <Icon active name="cloud-circle" />
              </Button>
               
            </Left>
            <Body>
                <Text>Kumar Pratik</Text>
                <Text note>Doing what you like will always keep you happy . .</Text>
              </Body>
              <Right>
                <Text note>3:43 pm</Text>
                
              </Right>
          </ListItem> */}

            <Loader loading={this.state.isLoading} color="#ffd04d" />
            {
                        this.state.data.map((y,i) => {

                            return (
                                <View>
                                <ListItem thumbnail style={{backgroundColor:'white', marginTop:5,marginBottom:3,}} onPress={()=> Actions.chiefdetail({values:y.idInc}) } >
                                   <View style={{marginLeft:10,left:10,}}>
                                       {/* <View style={{borderRightWidth:1, borderRightColor:'grey'}}/> */}
                                       <Text style={{fontFamily: "Roboto-Bold", fontSize:6,}}>Date</Text>
                                       <Text style={{fontFamily: "Roboto-bold", fontSize:12}}>{moment(y.date).format('D/MM/YYYY')}</Text>
                                       <Text style={{fontFamily: "Roboto-bold", fontSize:6,}}>Date</Text>
                                       <Text style={{fontFamily: "Roboto-bold", fontSize:12,}}>{moment(y.time).format('h:mm:ss')}</Text>
                                   </View>
                        
                                   <View style={{marginLeft:20,}}>
                        
                                       <Button style={{ backgroundColor: "transparent" }}>
                                               <Icon active name="cloud-circle" style={{color:'black'}}/>
                                       </Button>
                                       
                                   </View>
                                   <View style={{paddingLeft:20,}}>
                                       {/* <Icon active name="cloud-circle" /> */}
                                       <Text style={{fontFamily: "Roboto-bold", fontSize:6,padding:0}}>Incident</Text>
                                       <Text style={{fontFamily: "Roboto-bold", fontSize:12,padding:0}}>MEROKOK</Text>
                                       <Text style={{fontFamily: "Roboto-bold", fontSize:6,padding:0}}>Area Code</Text>
                                       <Text style={{fontFamily: "Roboto-bold", fontSize:12,padding:0}}>{y.location}</Text>
                                   </View>
                        
                        
                                   <View style={{paddingLeft:0,}}>
                                       {/* <Icon active name="cloud-circle" /> */}
                                       <Text style={{left:30,fontFamily: "Roboto-bold", fontSize:6,padding:0}}>Ref no</Text>
                                       <Text style={{left:30,fontFamily: "Roboto-bold", fontSize:12,padding:0}}>{y.idUser}</Text>
                                       <Text style={{left:30,fontFamily: "Roboto-bold", fontSize:6,padding:0}}>SPV</Text>
                                       <Text style={{left:30,fontFamily: "Roboto-bold", fontSize:12,padding:0}}>{y.name}</Text>
                                   </View>

                                   <Right style={{marginLeft:0,}}>
                                       <Icon  name="arrow-dropright" style={{left:70,}} />
                                   </Right>
                                   </ListItem>
                                </View>                    
                               )
                            
                        }) 
            }
           

            
            {/* SOS */}
           

         

         </Content>

    <AppFooter />
    </Container>
    );
  }
}

const styles = StyleSheet.create({
  gridView: {
    paddingTop: 25,
    paddingBottom:20,
    flex: 1,
    marginBottom:20,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 150,
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
});
