import React, { Component, } from 'react';
import { StyleSheet,Image, Dimensions, View, Text, TouchableHighlight, Alert,TextInput } from 'react-native';
import { Container, Left, Body, Right, Button, Title, Footer, FooterTab, Content, List, ListItem} from 'native-base';
import { Icon, Header,Input} from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import AppFooter from '../../appfooter';
import { RNCamera as Camera } from 'react-native-camera';
// import Icon from 'react-native-vector-icons'


export default class CameraOpen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      path: null,
    };
  }

  takePicture = async () => {
    try {
      const options = { quality: 0.5, base64: true, mirrorImage: true };
      const data = await this.camera.takePictureAsync(options);
      
      this.setState({ path: data.uri, metaimage:data });
      // this.props.updateImage(data.uri);
    //   console.log('Path to image: ' + data);
    console.log(data)
    } catch (err) {
      console.log('err: ', err);
    }
  };

  renderCamera() {
    return (
      <Camera
        ref={(cam) => {
          this.camera = cam;
        }}
        style={styles.preview}
        flashMode={Camera.Constants.FlashMode.off}
        permissionDialogTitle={'Permission to use camera'}
        permissionDialogMessage={'We need your permission to use your camera phone'}
      >
        <TouchableHighlight
          style={styles.capture}
          onPress={this.takePicture.bind(this)}
          underlayColor="rgba(255, 255, 255, 0.5)"
        >
          <View />
        </TouchableHighlight>
      </Camera>
    );
  }

  renderImage() {
    return (
      <View>
        <Image
          source={{ uri: this.state.path }}
          style={styles.preview}
        />
        <Text
          style={styles.cancel}
          onPress={() => this.setState({ path: null })}
        >Cancel
        </Text>

        <Text
          style={styles.uses}
          onPress={() => Actions.reportform({path:this.state.path, metaimage:this.state.metaimage, category:this.props.category}) }
        > Use
        </Text>

      </View>
    );
  }


  
  render() {
    return (
      <View style={styles.container}>
        {this.state.path ? this.renderImage() : this.renderCamera()}
      </View>
    );
  }
}

  


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#000',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width
  },
  capture: {
    width: 70,
    height: 70,
    borderRadius: 35,
    borderWidth: 5,
    borderColor: '#FFF',
    marginBottom: 15,
  },
  cancel: {
    position: 'absolute',
    right: 20,
    top: 20,
    backgroundColor: 'transparent',
    color: '#FFF',
    fontWeight: '600', 
    fontSize: 17,
  },
  uses: {
    position: 'absolute',
    left: 30,
    top: 20,
    backgroundColor: 'transparent',
    color: '#FFF',
    fontWeight: '600',
    fontSize: 17,
  }
});