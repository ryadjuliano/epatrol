import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, AsyncStorage, TextInput, Dimensions, Image } from 'react-native';
import { Container, Left, Body, Right, Title, Footer, FooterTab, Content } from 'native-base';

import { Header, Input, Card, ListItem, Button, Icon } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import MapView, { Polyline } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import { IncidentStore,HeaderCounter, GetScheduleTime } from '../constants/services';
import AppFooter from '../../appfooter';
import AppHeader from '../../appheader';
import Loader from 'react-native-modal-loader';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;

const LATITUDE = 1.065670;
const LONGITUDE = 104.132383;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const GOOGLE_MAPS_APIKEY = 'AIzaSyDOwIoYon9TVhqpFASALkEZfvSq7acI7gY';
const origin = { latitude: 1.0653160096281624, longitude: 104.13297845040131 };
const destination = { latitude: 1.06595962845581, longitude: 104.13065029297638 };



class Formsos extends Component {
  constructor(props) {
    super(props);

    // AirBnB's Office, and Apple Park
    this.state = {
      dataTime:'',
      coordinates: [
        {
          latitude: 1.073143,
          longitude: 104.131308,
        },
        {
          latitude: 1.073143,
          longitude: 104.131308,
        },
        {
          latitude: 1.073143,
          longitude: 104.131308,
        },
        {
          latitude: 1.073143,
          longitude: 104.131308,
        },
        {
          latitude: 1.073143,
          longitude: 104.131308,
        },
      ],
    };

    state = {
      isLoading: false
    };
    
    this.mapView = null;
  }

  

  showLoader = () => {
    this.setState({ isLoading: true });
  };

  GETTIMEPATROL = async () => {

    const UserData = {

      kondisi: 'getTimeSchedule',

    }

    // console.log(UserData)

    GetScheduleTime(UserData).then((result) => {
      console.log('kenapa',result.res[0].time_start)

      this.setState({
        dataTime: result.res[0].time_start,
        alur: result.res[0].alur,
        // dataTime: result.res[0].time_start
      })
    });
  }

  SendSOS = async () => {
    this.setState({ isLoading: true });
    // alert('helo')
    const id = await AsyncStorage.getItem("idUser")
    const params = {
      userid: id,
      lokasi: '',
      deskripsi: 'ini darurat',
      // image: r+'.jpg',
      kategori: '8',
      kondisi: 'incident'
    }

    // console.log(UserData)

    IncidentStore(params).then((result) => {
      console.log(result)
      if (result.res.status == 1) {
        alert('Success send  data')
        this.setState({ isLoading: false });
      }
    });
  }


  HeaderComponentCountData = () => {
    const params = {
      kondisi: 'CountHeader'
    }
    console.log(params)
    HeaderCounter(params).then((result) => {
      console.log(result.res)
      this.setState({
                incidents:result.res.data.jumlah_incident,
                sos:result.res.sos.jumlah_sos,
              })
      // this.setState({data:result.res})
    });

  }

  // GetDataMaps
  componentDidMount() {
    // this.GetDataMaps()
    this.HeaderComponentCountData()
    this.GETTIMEPATROL()
  }

  render() {
    return (
      <Container>
        <AppHeader />


<View style={{ backgroundColor: 'black', width: '100%', height: 20, }}>

<View style={{ flex: 1, flexDirection: 'row' }}>
  <Text style={{ color: '#fff', marginLeft: 10, }}>Incident(s):{this.state.incidents}</Text>
  <Text style={{ color: '#fff', marginLeft: 10, }}>Cleared:0</Text>
  <Text style={{ color: '#fff', marginLeft: 150, }}>SOS:{this.state.sos}</Text>
</View>
</View>

        <View style={{ flex: 1 }}>
          <Card
            title='WARNING'
            image={require('../../images/sos.png')} 
            imageStyle={{width:100,height:100, left:110 ,justifyContent:'center', alignItems:'center', color:'red'}}
            titleStyle={{color:'red'}}
            >
            <Text style={{ marginBottom: 10, color:'red', textAlign:'center' }}>
              Are you sure you want to send SOS ?
            </Text>

            <Button
              icon={<Icon name='send' color='#ffffff' />}
              backgroundColor='#03A9F4'
              buttonStyle={{ borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0 }}
              title=' SEND' 
              onPress={this.SendSOS}/>
              
          </Card>
          <Loader loading={this.state.isLoading} color="#ffd04d" />

        </View>

        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between',  }}>
          <View style={{ width: 50, height: 50, backgroundColor: '' }} />
          <View style={{ width: 50, height: 50, backgroundColor: '' }} />
          <View style={{ width: "85%", height: 80, backgroundColor: '#edeef2', marginBottom: 20, marginLeft: 20, marginRight: 20, }} >
            <View style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between'

            }}>
              <View style={{ width: null, height: null, backgroundColor: '' }} >
                <Text style={{ marginTop: 10, color: 'black', fontSize: 10,marginLeft: 50 }}>
                  Next Schedule start in
                </Text>
                <Text style={{ marginLeft: 30, fontSize: 36, }}>
                  {this.state.dataTime}
           </Text>
                

              </View>
              <View style={{ width: 50, height: 50, backgroundColor: 'transparent' }} >
                  <Text style={{ marginTop: 10, color: 'black', fontSize: 6,}}>
                      Patrol Type
                  </Text>
                  <Text style={{ marginTop: 0, color: 'black', fontSize: 10,fontWeight:'bold'}}>
                      {this.state.alur}
                  </Text>
                  {/* <Text style={{ marginTop: 10, color: 'black', fontSize: 6,}}>
                      Starting Point
                  </Text>
                  <Text style={{ marginTop: 0, color: 'black', fontSize: 10, fontWeight:'bold'}}>
                      B03
                  </Text> */}
                </View>
                <View style={{ width: 50, height: 50, backgroundColor: 'transparent' }} >
                <Text style={{ marginTop: 10, color: 'black', fontSize: 6,}}>
                      Total Distance
                  </Text>
                  <Text style={{ marginTop: 0, color: 'black', fontSize: 10,fontWeight:'bold'}}>
                      1000m
                  </Text>
                  <Text style={{ marginTop: 10, color: 'black', fontSize: 6,}}>
                      Estimated Time
                  </Text>
                  <Text style={{ marginTop: 0, color: 'black', fontSize: 10, fontWeight:'bold'}}>
                      50min
                  </Text>
                </View>

            </View>

          </View>
        </View>

        <AppFooter />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  gridView: {
    paddingTop: 25,
    paddingBottom: 20,
    flex: 1,
    marginBottom: 20,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 150,
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
  textInput: {
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    width: null,
    height: 40,
    borderColor: "#000",
    borderWidth: 0,
    borderRadius: 10,
    fontSize: 14,
    textAlign: "center",
    backgroundColor: "#edeef2",
    opacity: 1
  },
  textInputArea: {
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    width: null,
    height: 80,
    borderColor: "#000",
    borderWidth: 0,
    borderRadius: 10,
    fontSize: 14,
    textAlign: "center",
    backgroundColor: "#edeef2",
    opacity: 1
  },
  btnLogin: {
    marginTop: 10,
    marginBottom: 30,
    marginLeft: 10,
    width: 130,
    height: 30,
    borderRadius: 13,
    alignItems: 'center',
    backgroundColor: '#000'
  },
  btnText: {
    textAlign: 'center',
    padding: 10,
    color: 'white',
    fontSize: 10,
    fontWeight: 'bold'
  },
  btnSend: {
    marginTop: 10,
    marginBottom: 30,
    marginLeft: 10,
    marginRight: 10,
    width: null,
    height: 50,
    borderRadius: 13,
    alignItems: 'center',
    backgroundColor: '#000'
  },
  btnTexts: {
    textAlign: 'center',
    padding: 15,
    color: 'white',
    fontSize: 17,
    fontWeight: 'bold'
  },
  containermp: {
    ...StyleSheet.absoluteFillObject,
    height: 0,
    width: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginTop: 100,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
export default Formsos;
