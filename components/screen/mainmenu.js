import React, { Component } from 'react';
import { StyleSheet, BackHandler, View, Text, AsyncStorage,TouchableOpacity, Alert, Image } from 'react-native';
// import GridView from 'react-native-super-grid';
import { FlatGrid } from 'react-native-super-grid';
import { Container, Left,Icon,  List, ListItem,Body, Right, Button, Title, Footer, FooterTab, Content, CardItem, Thumbnail } from 'native-base';
import { Card, Header, Divider } from 'react-native-elements';
import ImageSliderz from 'react-native-image-slideshow';
import { Actions } from 'react-native-router-flux';
import { GetListIncidentGuard, HeaderCounter,UpdatePoistion } from './constants/services'
import AppFooter from './../appfooter'
// import Icon from 'react-native-vector-icons'
import Loader from 'react-native-modal-loader'; 

import AppHeader from '../appheader';
import Geolocation from '@react-native-community/geolocation';
var moment = require('moment');

export default class Example extends Component {


  constructor(props) {
    super(props)
    this.state = {
      data: [],
      incidents:null,
      sos:null
    }
    state = {
      isLoading: false,
      initialPosition: 'unknown',
      lastPosition: 'unknown',
      watchID: null,
    };
  }





        GetPositionData = async () => {
          const  idUser = await AsyncStorage.getItem("idUser")
          const  pola = await AsyncStorage.getItem("pola")
          Geolocation.getCurrentPosition(
              position => {
                  const initialPosition = JSON.stringify(position);
                  this.setState({ initialPosition });
              },
              error => Alert.alert('Error', JSON.stringify(error)),
              { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000, accuracy: 10 },
              );
              this.watchID = Geolocation.watchPosition(position => {
              const lastPosition = JSON.stringify(position);
              this.setState({ lastPosition });

              console.log('-->',position.coords.latitude)
              console.log('-->',position.coords.longitude)
              const params = {
                  "lati" : position.coords.latitude,
                  "longi" : position.coords.longitude,
                  "idUser" : idUser,
                  "kondisi": "positionUpdate",
                  "pola" :pola,
              }
              console.log('iniparams dari posisi',params)
              UpdatePoistion(params).then((result) => {
                console.log(result)
                // this.setState({
                //           incidents:result.res.data.jumlah_incident,
                //           sos:result.res.sos.jumlah_sos,
                //         })
                // this.setState({data:result.res})
              });



        });




        }

  showLoader = () => {
    this.setState({ isLoading: true });
  };


  IncidentList = () => { 
    const params = {
      kondisi: 'getDashboard'
    }
    console.log(params)
    GetListIncidentGuard(params).then((result) => {
      console.log(result)
      if (result.res === null) {
        alert('No Data')
        this.setState({ isLoading: false});
      } else {
        this.setState({ isLoading: false, data:result.res });
      }
      // this.setState({})
    });

  }

  HeaderComponentCountData = () => {
    const params = {
      kondisi: 'CountHeader'
    }
    console.log(params)
    HeaderCounter(params).then((result) => {
      console.log(result.res)
      this.setState({
                incidents:result.res.data.jumlah_incident,
                sos:result.res.sos.jumlah_sos,
              })
      // this.setState({data:result.res})
    });

  }



  componentDidMount() {
    this.setState({ isLoading: true });
    this.HeaderComponentCountData()
    this.IncidentList()
    this.GetPositionData()
  }
  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);

    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
        this.watchID != null && Geolocation.clearWatch(this.watchID);
    }

    handleBackButtonClick() {
      BackHandler.exitApp();
    }


  render() {
    // Taken from https://flatuicolors.com/
    const items = [
      { name: 'PENGANTAR', code: '#2ecc71' }, { name: 'DAFTAR ISI', code: '#2ecc71', key: "menu1" },
      { name: 'SAMBUTAN', code: '#2ecc71' }, { name: 'STRUKTUR ORGANISASI', code: '#2ecc71' },
      { name: 'DAFTAR ISI', code: '#2ecc71' }, { name: 'DATA PENDUDUK', code: '#2ecc71', action: 'onPress=this.' },
      // { name: 'NEPHRITIS', code: '#27ae60' }, { name: 'BELIZE HOLE', code: '#2980b9' },
      // { name: 'WISTERIA', code: '#8e44ad' }, { name: 'MIDNIGHT BLUE', code: '#2c3e50' },
      // { name: 'SUN FLOWER', code: '#f1c40f' }, { name: 'CARROT', code: '#e67e22' },
      // { name: 'ALIZARIN', code: '#e74c3c' }, { name: 'CLOUDS', code: '#ecf0f1' },
      // { name: 'CONCRETE', code: '#95a5a6' }, { name: 'ORANGE', code: '#f39c12' },
      // { name: 'PUMPKIN', code: '#d35400' }, { name: 'POMEGRANATE', code: '#c0392b' },
      // { name: 'SILVER', code: '#bdc3c7' }, { name: 'ASBESTOS', code: '#7f8c8d' },
    ];

    return (
      <Container style={{backgroundColor:'white'}}>

     <AppHeader />



        <View style={{ backgroundColor: 'black', width: '100%', height: 20, }}>

          <View style={{ flex: 1, flexDirection: 'row' }}>
            <Text style={{ color: '#fff', marginLeft: 10, }}>Incident(s):{this.state.incidents}</Text>
            <Text style={{ color: '#fff', marginLeft: 10, }}>Cleared:0</Text>
            <Text style={{ color: '#fff', marginLeft: 150, }}>SOS:{this.state.sos}</Text>
          </View>
        </View>
        <Loader loading={this.state.isLoading} color="#ffd04d" />
        <Content>
                  {     
                        this.state.data.map((y) => {
                          var ab = moment(y.date).format('D MMM, YYYY')
                          return (

                            <ListItem thumbnail style={{marginLeft:0}}>
                              {y.category == 8 ?

                            <View style={{borderRadius:8, marginLeft:10,backgroundColor:'#be1e2d',marginBottom:5, marginRight:10, height:35 ,}}>
                            <Text style={{ marginTop:8, justifyContent:'center', alignItems:'center', left:5, right:0,fontFamily: "Roboto-Bold", fontSize:10, color:'white'}}>{ab} {y.time} {y.name}  reported {y.decsription} @ {y.location}</Text>
                            </View>
                              :
                              <View style={{borderRadius:8, marginLeft:10,backgroundColor:'#e6e7e8',marginBottom:5, marginRight:10, height:35 ,}}>
                                   <Text style={{ marginTop:8, justifyContent:'center', alignItems:'center', left:5, right:0,fontFamily: "Roboto-Bold", fontSize:10, color:'black'}}>{ab} {y.time} {y.name}  reported {y.decsription} @ {y.location}</Text>
                              </View>



                              }

                               {/* <View style={{flex:1,flexDirection:'row', justifyContent:'flex-end', left:0,}}>
                                  <Icon  name="camera" />
                               </View> */}
                               <Right style={{left:20,}}>
                                       <Icon  name="camera" />
                                </Right>
                                <Right style={{left:7,}}>
                                       <Icon  name="videocam" />
                                </Right>
                              </ListItem>
                           );
                        })
                  }
        </Content>
        <AppFooter />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  gridView: {
    paddingTop: 25,
    paddingBottom: 20,
    flex: 1,
    marginBottom: 20,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 150,
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
});
