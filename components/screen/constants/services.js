// var Url = "http://barbeka.com/android/api-v1.php";
var Url = "http://165.22.63.165:2000/android/api-v1.php";


export function PostData(userData) { 
    let BaseURL = Url
    return new Promise((resolve, reject) => {
        fetch(BaseURL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }, 
            body: JSON.stringify(userData)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                resolve(responseJson);
                // console.log('asd',responseJson)
            })
            .catch((error) => {
              reject(error)
              console.log(error)
            });
    });
}

export function OutLogCheck(userData) { 
    let BaseURL = Url
    return new Promise((resolve, reject) => {
        fetch(BaseURL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }, 
            body: JSON.stringify(userData)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                resolve(responseJson);
                // console.log('asd',responseJson)
            })
            .catch((error) => {
              reject(error)
              console.log(error)
            });
    });
}

export function UpdatePoistion(userData) { 
    let BaseURL = Url
    return new Promise((resolve, reject) => {
        fetch(BaseURL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }, 
            body: JSON.stringify(userData)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                resolve(responseJson);
                // console.log('asd',responseJson)
            })
            .catch((error) => {
              reject(error)
              console.log(error)
            });
    });
}

export function updateTrackingLocation(userData) { 
    let BaseURL = Url
    return new Promise((resolve, reject) => {
        fetch(BaseURL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }, 
            body: JSON.stringify(userData)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                resolve(responseJson);
                // console.log('asd',responseJson)
            })
            .catch((error) => {
              reject(error)
              console.log(error)
            });
    });
}



export function GetMapsRoute(UserData) {
  console.log(UserData)
    let BaseURL = Url
    return new Promise((resolve, reject) => {
        fetch(BaseURL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(UserData)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                resolve(responseJson);
                // console.log('asd',responseJson)
            })
            .catch((error) => {
              reject(error)
              console.log(error)
            });
    });
}

export function GetScheduleTime(UserData) {
    console.log(UserData)
      let BaseURL = Url
      return new Promise((resolve, reject) => {
          fetch(BaseURL, {
              method: 'POST',
              headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
              },
              body: JSON.stringify(UserData)
          })
              .then((response) => response.json())
              .then((responseJson) => {
                  resolve(responseJson);
                  // console.log('asd',responseJson)
              })
              .catch((error) => {
                reject(error)
                console.log(error)
              });
      });
  }

  export function GuarOnlineTracking(UserData) {
    console.log(UserData)
      let BaseURL = Url
      return new Promise((resolve, reject) => {
          fetch(BaseURL, {
              method: 'POST',
              headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
              },
              body: JSON.stringify(UserData)
          })
              .then((response) => response.json())
              .then((responseJson) => {
                  resolve(responseJson);
                  // console.log('asd',responseJson)
              })
              .catch((error) => {
                reject(error)
                console.log(error)
              });
      });
  }


export function IncidentStore(userData) {
    let BaseURL = Url
    console.log(userData)
    return new Promise((resolve, reject) => {
        fetch(BaseURL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(userData)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                resolve(responseJson);
                // console.log('asd',responseJson)
            })
            .catch((error) => {
                console.log(error)
            //   reject(error)
            //   console.log(error)
            });
    });
}


export function GetListIncidentGuard(userData) {
    let BaseURL = Url
    return new Promise((resolve, reject) => {
        fetch(BaseURL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(userData)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                resolve(responseJson);
                // console.log('asd',responseJson)
            })
            .catch((error) => {
              reject(error)
              console.log(error)
            });
    });
}

export function HeaderCounter(userData) {
    let BaseURL = Url
    return new Promise((resolve, reject) => {
        fetch(BaseURL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(userData)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                resolve(responseJson);
                // console.log('asd',responseJson)
            })
            .catch((error) => {
              reject(error)
              console.log(error)
            });
    });
}

export function CheckInPoint(userData) {
    let BaseURL = Url
    return new Promise((resolve, reject) => {
        fetch(BaseURL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(userData)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                resolve(responseJson);
                // console.log('asd',responseJson)
            })
            .catch((error) => {
              reject(error)
              console.log(error)
            });
    });
}

export function GetNewDataMap(userData) {
    let BaseURL = Url
    return new Promise((resolve, reject) => {
        fetch(BaseURL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(userData)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                resolve(responseJson);
                // console.log('asd',responseJson)
            })
            .catch((error) => {
              reject(error)
              console.log(error)
            });
    });
}

export function IncidentQueryList(userData) {
    let BaseURL = Url
    return new Promise((resolve, reject) => {
        fetch(BaseURL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(userData)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                resolve(responseJson);
                // console.log('asd',responseJson)
            })
            .catch((error) => {
              reject(error)
              console.log(error)
            });
    });
}


