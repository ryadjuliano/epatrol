import React, { Component } from 'react';
import { Image } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';


export default class Planning extends Component {
  render() {
    return (
      <Container>
        <Content>
          <Card  style={{marginTop:30,  marginLeft:13, marginRight:13, borderRadius:8,}}>

            <CardItem cardBody>
              <Image source={{uri: 'https://samuelbonaparte.id/wp-content/uploads/2018/08/SamuelBonaparte.jpg'}} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>

            <CardItem footer bordered >
                <Left>
                    <Body >
                        <Text note style={{ fontSize: 12, fontWeight: 'bold' }}>GO OPEN MIND</Text>
                        <Text style={{ fontSize: 17, fontWeight: 'bold' }}>Kesejahteraan Rakyat No 1</Text>
                    </Body>
                </Left>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}
