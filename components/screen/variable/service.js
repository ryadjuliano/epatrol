import { AlertIOS } from 'react-native'

import { Actions }  from 'react-native-router-flux'


export function LoginData(userData) {

    let BaseURL = GLOBAL.UrlConfig +'auth';
    return new Promise((resolve, reject) => {
        fetch(BaseURL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Accept-Encoding' :'application/json',
            },
            body: JSON.stringify(userData)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                resolve(responseJson);
            })
            .catch((error) => {
                if (error) {
                    // this.refs.loading.close();
                    // this.
                    alert('Failed Validate Membership: Your membership id or password is wrong ')
                    // this.setState({
                    //     spinner: false,
                    // })
                }
            });
    });
}
export function GetMapsRoute(userData) {

    let BaseURL = GLOBAL.UrlConfig +'register';
    return new Promise((resolve, reject) => {
        fetch(BaseURL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Accept-Encoding' :'application/json',
            },
            body: JSON.stringify(userData)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                resolve(responseJson);
            })
            .catch((error) => {
                if (error) {
                    // this.refs.loading.close();
                    // this.
                    alert('Failed Validate Membership: Your membership id or password is wrong ')
                    // this.setState({
                    //     spinner: false,
                    // })
                }
            });
    });
}

export function GetListCustomerArea(userData) {

    const api_token = userData.api_token

    let BaseURL = GLOBAL.UrlConfig +'customer?api_token='+ api_token;
    console.log(api_token)
    return new Promise((resolve, reject) => {
        fetch(BaseURL, {
            method: 'GET',
            headers: {
                // 'Accept': 'application/json',
                // 'Content-Type': 'application/json',
                'Accept-Encoding' :'application/json',
            },
            // body: JSON.stringify(userData)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                resolve(responseJson);
            })
            .catch((error) => {
                if (error) {
                    console.log(error)
                }
            });
    });
}

// AREA INFORMATION START

export function Office(userData) {

    const api_token = userData.api_token

    let BaseURL = GLOBAL.UrlConfig +'office?api_token='+ api_token;
    console.log(api_token)
    return new Promise((resolve, reject) => {
        fetch(BaseURL, {
            method: 'GET',
            headers: {
                // 'Accept': 'application/json',
                // 'Content-Type': 'application/json',
                'Accept-Encoding' :'application/json',
            },
            // body: JSON.stringify(userData)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                resolve(responseJson);
            })
            .catch((error) => {
                if (error) {
                    console.log(error)
                }
            });
    });
}

export function Province(userData) {

    const api_token = userData.api_token

    let BaseURL = GLOBAL.UrlConfig +'province?api_token='+ api_token;
    console.log(api_token)
    return new Promise((resolve, reject) => {
        fetch(BaseURL, {
            method: 'GET',
            headers: {
                // 'Accept': 'application/json',
                // 'Content-Type': 'application/json',
                'Accept-Encoding' :'application/json',
            },
            // body: JSON.stringify(userData)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                resolve(responseJson);
            })
            .catch((error) => {
                if (error) {
                    console.log(error)
                }
            });
    });
}

export function City(userData) {

    const api_token = userData.api_token

    let BaseURL = GLOBAL.UrlConfig +'city?api_token='+ api_token;
    console.log(api_token)
    return new Promise((resolve, reject) => {
        fetch(BaseURL, {
            method: 'GET',
            headers: {
                // 'Accept': 'application/json',
                // 'Content-Type': 'application/json',
                'Accept-Encoding' :'application/json',
            },
            // body: JSON.stringify(userData)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                resolve(responseJson);
            })
            .catch((error) => {
                if (error) {
                    console.log(error)
                }
            });
    });
}

export function District(userData) {

    const api_token = userData.api_token

    let BaseURL = GLOBAL.UrlConfig +'district?api_token='+ api_token;
    console.log(api_token)
    return new Promise((resolve, reject) => {
        fetch(BaseURL, {
            method: 'GET',
            headers: {
                // 'Accept': 'application/json',
                // 'Content-Type': 'application/json',
                'Accept-Encoding' :'application/json',
            },
            // body: JSON.stringify(userData)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                resolve(responseJson);
            })
            .catch((error) => {
                if (error) {
                    console.log(error)
                }
            });
    });
}

export function SegmentData(userData) {

    const api_token = userData.api_token

    let BaseURL = GLOBAL.UrlConfig +'segment?api_token='+ api_token;
    console.log(api_token)
    return new Promise((resolve, reject) => {
        fetch(BaseURL, {
            method: 'GET',
            headers: {
                // 'Accept': 'application/json',
                // 'Content-Type': 'application/json',
                'Accept-Encoding' :'application/json',
            },
            // body: JSON.stringify(userData)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                resolve(responseJson);
                console.log(responseJson)
            })
            .catch((error) => {
                if (error) {
                    console.log(error)
                }
            });
    });
}




// AREA INFORMATION END

// INSERT / UPDATE CUSTOMER

export function SubmitCustomerAdd(userData) {

    let BaseURL = GLOBAL.UrlConfig +'customer';
    return new Promise((resolve, reject) => {
        fetch(BaseURL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Accept-Encoding' :'application/json',
            },
            body: JSON.stringify(userData)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                resolve(responseJson);
            })
            .catch((error) => {
                if (error) {
                    // this.refs.loading.close();
                    // this.
                    alert('Failed Validate Membership: Your membership id or password is wrong ')
                    // this.setState({
                    //     spinner: false,
                    // })
                }
            });
    });
}
// END INSERT / UPDATE CUSTOMER
